import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-word',
  templateUrl: './word.component.html',
  styleUrls: ['./word.component.css']
})
export class WordComponent implements OnInit {
  editField: string;
  arrCustomer = [
    { id: 1, IDLKH: 'BK', TENLKH: 'Nhóm khách hàng bánh kẹo', GHICHU: 'Nhóm khách hàng bánh kẹo' },
    { id: 2, IDLKH: 'MK', TENLKH: 'Nhóm khách hàng sữa', GHICHU: '' },
    { id: 3, IDLKH: 'CN', TENLKH: 'Nhóm khách hàng thức ăn chăn nuôi', GHICHU: '' },
    { id: 4, IDLKH: 'LE', TENLKH: 'Nhóm khách hàng lẻ', GHICHU: '' },
    { id: 5, IDLKH: 'TS', TENLKH: 'Nhóm khách hàng Trà Sữa', GHICHU: '' }
  ];

  newIDLKH = '';
  newTENLKH = '';
  newGHICHU = '';

  constructor() { }

  ngOnInit() {
  }

  addCostumer() {
    this.arrCustomer.push({
      id: this.arrCustomer.length + 1,
      IDLKH: this.newIDLKH,
      TENLKH: this.newTENLKH,
      GHICHU: this.newGHICHU,
    });
  }
  deleteNew(id: number) {
    let confirmResult = confirm("Bạn có chắc muốn xóa dữ liệu!");
    const index = this.arrCustomer.findIndex(e => e.id == id);
    if (confirmResult) {
      this.arrCustomer.splice(index, 1);
    }
  }

}
